"""
3. WAP to find the 7th odd number (start from 1)

Output: 7th odd number is 13"""

num = int(input("Enter The Number: "))
i = 0
count = 0

while i <= num*2:
    if i % 2 != 0:
        count += 1
        if count == num :
            print("%dth Odd Nuber is %d"%(num,i))
            break
    i += 1
