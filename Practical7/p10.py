"""

10. Write a program to check whether the number is a Strong
number or not.
Input: 145
Output: 145 is Strong number

"""

num = int(input("Enter Number: "))
rem = 0
num2 = num
sum1 = 0

while num > 0:

    rem = num % 10

    i = 1
    while rem != 0:
        i *= rem
        rem -= 1

    sum1 += i

    num = num // 10

if (sum1 == num2):
    print(num2,"is a Strong Number")
else:
    print(num2,"is not a Strong Number")


