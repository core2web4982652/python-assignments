"""2. Write a program to check whether the number is a Prime
number or not. (2332)
Input:2332
Output:2332 is not a prime number"""

num = int(input("Enter Number: "))
count = 0

i = 1

while i <= num:

    if num % i == 0:
        count += 1
    i += 1

if count == 2:
    print("%d is a Prime Number"%(num))
else:
    print("%d is not a Prime Number"%(num))
