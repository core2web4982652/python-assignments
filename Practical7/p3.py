"""
3. Write a program to check whether the number is a Perfect
number or not.
Input: 496
Output: 496 is a perfect number."""

num = int(input("Enter Number: "))
sum = 0

i = 1

while i < num:

    if num % i == 0:
        sum += i
    i += 1

if sum == num:
    print("%d is a Perfect Number"%(num))
else: 
    print("%d is not a Perfect Number"%(num))

