"""
4. Write a program to count the digits of the given number.
Input:942111423
Output: 9"""

num = int(input("Enter Number: "))
rem = 0
count = 0

while num > 0:

    rem = num % 10

    count += 1

    num = num // 10

print("count Of Given Number is %d"%(count))
