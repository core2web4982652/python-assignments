"""
6. Write a program to count the Odd digits of the given
number.
Input: 942111423
Output: 5"""

num = int(input("Enter Number: "))
rem = 0
count = 0


while num > 0:
    
    rem = num % 10

    if rem % 2 != 0:
        count += 1

    num //= 10

print("Count of Odd Numbers is: %d"%(count))
