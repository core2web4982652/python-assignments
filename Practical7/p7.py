"""
7. Write a program to reverse the given number.
Input:43521
Output: 12534"""

num = int(input("Enter Number: "))
rem = 0
rev = 0
num2 = num

while num > 0:

    rem = num % 10

    rev = rev*10 + rem

    num //= 10

print("Reverse Of %d Number is: %d"%(num2,rev))
