"""

8. Write a program to check whether the number is a
Palindrome number or not.
Input: 2332
Output: 2332 is a palindrome number

"""

num = int(input("Enter Number: "))
rem = 0
pal = 0
num2 = num

while num > 0:

    rem = num % 10

    pal = pal*10 + rem

    num //= 10

if pal == num2:
    print(pal,"is a Palindrom Number")
else: 
    print(pal,"is not a Palindrom Number")
